### A Pluto.jl notebook ###
# v0.11.12

using Markdown
using InteractiveUtils

# ╔═╡ 7b7a4ada-f08f-11ea-0bad-fb8333f8a0b4
begin
	using Distributions
	using StatsBase
	using Plots
end

# ╔═╡ ae3fdb5e-f08f-11ea-11b7-5ba243cc5eb1
begin
	n = 100_000
	m = 10
	p = 0.1
	x = rand(Normal(p*m,1), n)
	y = rand(Normal((1-p)*m,1), n)
end

# ╔═╡ 1b820296-f092-11ea-36d4-f94789a177ed
r = max.(x, y) ./ (x + y)

# ╔═╡ fb549d5c-f08f-11ea-2e69-0d11a12bc74f
mean(r)

# ╔═╡ 0f94006a-f092-11ea-199c-b1beff04477c
sqrt(var(r))

# ╔═╡ 3dc9ee9a-f092-11ea-2ca5-313d525525c2
histogram(r)

# ╔═╡ Cell order:
# ╠═7b7a4ada-f08f-11ea-0bad-fb8333f8a0b4
# ╠═ae3fdb5e-f08f-11ea-11b7-5ba243cc5eb1
# ╠═1b820296-f092-11ea-36d4-f94789a177ed
# ╠═fb549d5c-f08f-11ea-2e69-0d11a12bc74f
# ╠═0f94006a-f092-11ea-199c-b1beff04477c
# ╠═3dc9ee9a-f092-11ea-2ca5-313d525525c2
